import React from 'react'

type Props = {}

const App = (props: Props) => {

  // 正确实现数组中最大差值[1,6,7,9]=>[8]
  const finMax = (arr: number[]) => [Math.max(...arr) - Math.min(...arr)];
  const arr = [1, 6, 7, 9];
  const result = finMax(arr);
  console.log(result);

  const arr1 = [2, 1, 4, 3];
  console.log(arr1.sort());



  return (
    <div>App</div>
  )
}

export default App