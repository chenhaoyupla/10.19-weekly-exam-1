module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "node": true
    },
    "extends": [
        "standard-with-typescript",
        "plugin:react/recommended",
        "prettier"
    ],
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script",
                "project": "tsconfig.json"
            }
        }
    ],
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "plugins": [
        "react",
        "prettier"
    ],
    "rules": {
        "@typescript-eslint/consistent-type-definitions": 0,
        "@typescript-eslint/ban-types": 0,
        "@typescript-eslint/explicit-function-return-type": 0,
        "@typescript-eslint/require-array-sort-compare": 0,
    }
}
